//
// Generated file, do not edit! Created by opp_msgtool 6.0 from inet/applications/vehicular/MinosMCOPacket.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wshadow"
#  pragma clang diagnostic ignored "-Wconversion"
#  pragma clang diagnostic ignored "-Wunused-parameter"
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wunreachable-code-break"
#  pragma clang diagnostic ignored "-Wold-style-cast"
#elif defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wshadow"
#  pragma GCC diagnostic ignored "-Wconversion"
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  pragma GCC diagnostic ignored "-Wold-style-cast"
#  pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn"
#  pragma GCC diagnostic ignored "-Wfloat-conversion"
#endif

#include <iostream>
#include <sstream>
#include <memory>
#include <type_traits>
#include "MinosMCOPacket_m.h"

namespace omnetpp {

// Template pack/unpack rules. They are declared *after* a1l type-specific pack functions for multiple reasons.
// They are in the omnetpp namespace, to allow them to be found by argument-dependent lookup via the cCommBuffer argument

// Packing/unpacking an std::vector
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::vector<T,A>& v)
{
    int n = v.size();
    doParsimPacking(buffer, n);
    for (int i = 0; i < n; i++)
        doParsimPacking(buffer, v[i]);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::vector<T,A>& v)
{
    int n;
    doParsimUnpacking(buffer, n);
    v.resize(n);
    for (int i = 0; i < n; i++)
        doParsimUnpacking(buffer, v[i]);
}

// Packing/unpacking an std::list
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::list<T,A>& l)
{
    doParsimPacking(buffer, (int)l.size());
    for (typename std::list<T,A>::const_iterator it = l.begin(); it != l.end(); ++it)
        doParsimPacking(buffer, (T&)*it);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::list<T,A>& l)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        l.push_back(T());
        doParsimUnpacking(buffer, l.back());
    }
}

// Packing/unpacking an std::set
template<typename T, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::set<T,Tr,A>& s)
{
    doParsimPacking(buffer, (int)s.size());
    for (typename std::set<T,Tr,A>::const_iterator it = s.begin(); it != s.end(); ++it)
        doParsimPacking(buffer, *it);
}

template<typename T, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::set<T,Tr,A>& s)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        T x;
        doParsimUnpacking(buffer, x);
        s.insert(x);
    }
}

// Packing/unpacking an std::map
template<typename K, typename V, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::map<K,V,Tr,A>& m)
{
    doParsimPacking(buffer, (int)m.size());
    for (typename std::map<K,V,Tr,A>::const_iterator it = m.begin(); it != m.end(); ++it) {
        doParsimPacking(buffer, it->first);
        doParsimPacking(buffer, it->second);
    }
}

template<typename K, typename V, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::map<K,V,Tr,A>& m)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        K k; V v;
        doParsimUnpacking(buffer, k);
        doParsimUnpacking(buffer, v);
        m[k] = v;
    }
}

// Default pack/unpack function for arrays
template<typename T>
void doParsimArrayPacking(omnetpp::cCommBuffer *b, const T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimPacking(b, t[i]);
}

template<typename T>
void doParsimArrayUnpacking(omnetpp::cCommBuffer *b, T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimUnpacking(b, t[i]);
}

// Default rule to prevent compiler from choosing base class' doParsimPacking() function
template<typename T>
void doParsimPacking(omnetpp::cCommBuffer *, const T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimPacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

template<typename T>
void doParsimUnpacking(omnetpp::cCommBuffer *, T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimUnpacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

}  // namespace omnetpp

namespace inet {

Register_Class(MinosMCOPacket)

MinosMCOPacket::MinosMCOPacket() : ::inet::MCOPacket()
{
}

MinosMCOPacket::MinosMCOPacket(const MinosMCOPacket& other) : ::inet::MCOPacket(other)
{
    copy(other);
}

MinosMCOPacket::~MinosMCOPacket()
{
    delete [] this->channelWeigths;
    delete [] this->channelCBT;
}

MinosMCOPacket& MinosMCOPacket::operator=(const MinosMCOPacket& other)
{
    if (this == &other) return *this;
    ::inet::MCOPacket::operator=(other);
    copy(other);
    return *this;
}

void MinosMCOPacket::copy(const MinosMCOPacket& other)
{
    delete [] this->channelWeigths;
    this->channelWeigths = (other.channelWeigths_arraysize==0) ? nullptr : new double[other.channelWeigths_arraysize];
    channelWeigths_arraysize = other.channelWeigths_arraysize;
    for (size_t i = 0; i < channelWeigths_arraysize; i++) {
        this->channelWeigths[i] = other.channelWeigths[i];
    }
    delete [] this->channelCBT;
    this->channelCBT = (other.channelCBT_arraysize==0) ? nullptr : new double[other.channelCBT_arraysize];
    channelCBT_arraysize = other.channelCBT_arraysize;
    for (size_t i = 0; i < channelCBT_arraysize; i++) {
        this->channelCBT[i] = other.channelCBT[i];
    }
}

void MinosMCOPacket::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::inet::MCOPacket::parsimPack(b);
    b->pack(channelWeigths_arraysize);
    doParsimArrayPacking(b,this->channelWeigths,channelWeigths_arraysize);
    b->pack(channelCBT_arraysize);
    doParsimArrayPacking(b,this->channelCBT,channelCBT_arraysize);
}

void MinosMCOPacket::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::inet::MCOPacket::parsimUnpack(b);
    delete [] this->channelWeigths;
    b->unpack(channelWeigths_arraysize);
    if (channelWeigths_arraysize == 0) {
        this->channelWeigths = nullptr;
    } else {
        this->channelWeigths = new double[channelWeigths_arraysize];
        doParsimArrayUnpacking(b,this->channelWeigths,channelWeigths_arraysize);
    }
    delete [] this->channelCBT;
    b->unpack(channelCBT_arraysize);
    if (channelCBT_arraysize == 0) {
        this->channelCBT = nullptr;
    } else {
        this->channelCBT = new double[channelCBT_arraysize];
        doParsimArrayUnpacking(b,this->channelCBT,channelCBT_arraysize);
    }
}

size_t MinosMCOPacket::getChannelWeigthsArraySize() const
{
    return channelWeigths_arraysize;
}

double MinosMCOPacket::getChannelWeigths(size_t k) const
{
    if (k >= channelWeigths_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelWeigths_arraysize, (unsigned long)k);
    return this->channelWeigths[k];
}

void MinosMCOPacket::setChannelWeigthsArraySize(size_t newSize)
{
    handleChange();
    double *channelWeigths2 = (newSize==0) ? nullptr : new double[newSize];
    size_t minSize = channelWeigths_arraysize < newSize ? channelWeigths_arraysize : newSize;
    for (size_t i = 0; i < minSize; i++)
        channelWeigths2[i] = this->channelWeigths[i];
    for (size_t i = minSize; i < newSize; i++)
        channelWeigths2[i] = 0;
    delete [] this->channelWeigths;
    this->channelWeigths = channelWeigths2;
    channelWeigths_arraysize = newSize;
}

void MinosMCOPacket::setChannelWeigths(size_t k, double channelWeigths)
{
    if (k >= channelWeigths_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelWeigths_arraysize, (unsigned long)k);
    handleChange();
    this->channelWeigths[k] = channelWeigths;
}

void MinosMCOPacket::insertChannelWeigths(size_t k, double channelWeigths)
{
    if (k > channelWeigths_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelWeigths_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = channelWeigths_arraysize + 1;
    double *channelWeigths2 = new double[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        channelWeigths2[i] = this->channelWeigths[i];
    channelWeigths2[k] = channelWeigths;
    for (i = k + 1; i < newSize; i++)
        channelWeigths2[i] = this->channelWeigths[i-1];
    delete [] this->channelWeigths;
    this->channelWeigths = channelWeigths2;
    channelWeigths_arraysize = newSize;
}

void MinosMCOPacket::appendChannelWeigths(double channelWeigths)
{
    insertChannelWeigths(channelWeigths_arraysize, channelWeigths);
}

void MinosMCOPacket::eraseChannelWeigths(size_t k)
{
    if (k >= channelWeigths_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelWeigths_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = channelWeigths_arraysize - 1;
    double *channelWeigths2 = (newSize == 0) ? nullptr : new double[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        channelWeigths2[i] = this->channelWeigths[i];
    for (i = k; i < newSize; i++)
        channelWeigths2[i] = this->channelWeigths[i+1];
    delete [] this->channelWeigths;
    this->channelWeigths = channelWeigths2;
    channelWeigths_arraysize = newSize;
}

size_t MinosMCOPacket::getChannelCBTArraySize() const
{
    return channelCBT_arraysize;
}

double MinosMCOPacket::getChannelCBT(size_t k) const
{
    if (k >= channelCBT_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelCBT_arraysize, (unsigned long)k);
    return this->channelCBT[k];
}

void MinosMCOPacket::setChannelCBTArraySize(size_t newSize)
{
    handleChange();
    double *channelCBT2 = (newSize==0) ? nullptr : new double[newSize];
    size_t minSize = channelCBT_arraysize < newSize ? channelCBT_arraysize : newSize;
    for (size_t i = 0; i < minSize; i++)
        channelCBT2[i] = this->channelCBT[i];
    for (size_t i = minSize; i < newSize; i++)
        channelCBT2[i] = 0;
    delete [] this->channelCBT;
    this->channelCBT = channelCBT2;
    channelCBT_arraysize = newSize;
}

void MinosMCOPacket::setChannelCBT(size_t k, double channelCBT)
{
    if (k >= channelCBT_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelCBT_arraysize, (unsigned long)k);
    handleChange();
    this->channelCBT[k] = channelCBT;
}

void MinosMCOPacket::insertChannelCBT(size_t k, double channelCBT)
{
    if (k > channelCBT_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelCBT_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = channelCBT_arraysize + 1;
    double *channelCBT2 = new double[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        channelCBT2[i] = this->channelCBT[i];
    channelCBT2[k] = channelCBT;
    for (i = k + 1; i < newSize; i++)
        channelCBT2[i] = this->channelCBT[i-1];
    delete [] this->channelCBT;
    this->channelCBT = channelCBT2;
    channelCBT_arraysize = newSize;
}

void MinosMCOPacket::appendChannelCBT(double channelCBT)
{
    insertChannelCBT(channelCBT_arraysize, channelCBT);
}

void MinosMCOPacket::eraseChannelCBT(size_t k)
{
    if (k >= channelCBT_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)channelCBT_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = channelCBT_arraysize - 1;
    double *channelCBT2 = (newSize == 0) ? nullptr : new double[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        channelCBT2[i] = this->channelCBT[i];
    for (i = k; i < newSize; i++)
        channelCBT2[i] = this->channelCBT[i+1];
    delete [] this->channelCBT;
    this->channelCBT = channelCBT2;
    channelCBT_arraysize = newSize;
}

class MinosMCOPacketDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertyNames;
    enum FieldConstants {
        FIELD_channelWeigths,
        FIELD_channelCBT,
    };
  public:
    MinosMCOPacketDescriptor();
    virtual ~MinosMCOPacketDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyName) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyName) const override;
    virtual int getFieldArraySize(omnetpp::any_ptr object, int field) const override;
    virtual void setFieldArraySize(omnetpp::any_ptr object, int field, int size) const override;

    virtual const char *getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const override;
    virtual std::string getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const override;
    virtual omnetpp::cValue getFieldValue(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual omnetpp::any_ptr getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const override;
};

Register_ClassDescriptor(MinosMCOPacketDescriptor)

MinosMCOPacketDescriptor::MinosMCOPacketDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(inet::MinosMCOPacket)), "inet::MCOPacket")
{
    propertyNames = nullptr;
}

MinosMCOPacketDescriptor::~MinosMCOPacketDescriptor()
{
    delete[] propertyNames;
}

bool MinosMCOPacketDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<MinosMCOPacket *>(obj)!=nullptr;
}

const char **MinosMCOPacketDescriptor::getPropertyNames() const
{
    if (!propertyNames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
        const char **baseNames = base ? base->getPropertyNames() : nullptr;
        propertyNames = mergeLists(baseNames, names);
    }
    return propertyNames;
}

const char *MinosMCOPacketDescriptor::getProperty(const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? base->getProperty(propertyName) : nullptr;
}

int MinosMCOPacketDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? 2+base->getFieldCount() : 2;
}

unsigned int MinosMCOPacketDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeFlags(field);
        field -= base->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISARRAY | FD_ISEDITABLE | FD_ISRESIZABLE,    // FIELD_channelWeigths
        FD_ISARRAY | FD_ISEDITABLE | FD_ISRESIZABLE,    // FIELD_channelCBT
    };
    return (field >= 0 && field < 2) ? fieldTypeFlags[field] : 0;
}

const char *MinosMCOPacketDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldName(field);
        field -= base->getFieldCount();
    }
    static const char *fieldNames[] = {
        "channelWeigths",
        "channelCBT",
    };
    return (field >= 0 && field < 2) ? fieldNames[field] : nullptr;
}

int MinosMCOPacketDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    int baseIndex = base ? base->getFieldCount() : 0;
    if (strcmp(fieldName, "channelWeigths") == 0) return baseIndex + 0;
    if (strcmp(fieldName, "channelCBT") == 0) return baseIndex + 1;
    return base ? base->findField(fieldName) : -1;
}

const char *MinosMCOPacketDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeString(field);
        field -= base->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "double",    // FIELD_channelWeigths
        "double",    // FIELD_channelCBT
    };
    return (field >= 0 && field < 2) ? fieldTypeStrings[field] : nullptr;
}

const char **MinosMCOPacketDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldPropertyNames(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *MinosMCOPacketDescriptor::getFieldProperty(int field, const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldProperty(field, propertyName);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int MinosMCOPacketDescriptor::getFieldArraySize(omnetpp::any_ptr object, int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldArraySize(object, field);
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        case FIELD_channelWeigths: return pp->getChannelWeigthsArraySize();
        case FIELD_channelCBT: return pp->getChannelCBTArraySize();
        default: return 0;
    }
}

void MinosMCOPacketDescriptor::setFieldArraySize(omnetpp::any_ptr object, int field, int size) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldArraySize(object, field, size);
            return;
        }
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        case FIELD_channelWeigths: pp->setChannelWeigthsArraySize(size); break;
        case FIELD_channelCBT: pp->setChannelCBTArraySize(size); break;
        default: throw omnetpp::cRuntimeError("Cannot set array size of field %d of class 'MinosMCOPacket'", field);
    }
}

const char *MinosMCOPacketDescriptor::getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldDynamicTypeString(object,field,i);
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string MinosMCOPacketDescriptor::getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValueAsString(object,field,i);
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        case FIELD_channelWeigths: return double2string(pp->getChannelWeigths(i));
        case FIELD_channelCBT: return double2string(pp->getChannelCBT(i));
        default: return "";
    }
}

void MinosMCOPacketDescriptor::setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValueAsString(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        case FIELD_channelWeigths: pp->setChannelWeigths(i,string2double(value)); break;
        case FIELD_channelCBT: pp->setChannelCBT(i,string2double(value)); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'MinosMCOPacket'", field);
    }
}

omnetpp::cValue MinosMCOPacketDescriptor::getFieldValue(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValue(object,field,i);
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        case FIELD_channelWeigths: return pp->getChannelWeigths(i);
        case FIELD_channelCBT: return pp->getChannelCBT(i);
        default: throw omnetpp::cRuntimeError("Cannot return field %d of class 'MinosMCOPacket' as cValue -- field index out of range?", field);
    }
}

void MinosMCOPacketDescriptor::setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValue(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        case FIELD_channelWeigths: pp->setChannelWeigths(i,value.doubleValue()); break;
        case FIELD_channelCBT: pp->setChannelCBT(i,value.doubleValue()); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'MinosMCOPacket'", field);
    }
}

const char *MinosMCOPacketDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructName(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    };
}

omnetpp::any_ptr MinosMCOPacketDescriptor::getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructValuePointer(object, field, i);
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        default: return omnetpp::any_ptr(nullptr);
    }
}

void MinosMCOPacketDescriptor::setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldStructValuePointer(object, field, i, ptr);
            return;
        }
        field -= base->getFieldCount();
    }
    MinosMCOPacket *pp = omnetpp::fromAnyPtr<MinosMCOPacket>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'MinosMCOPacket'", field);
    }
}

}  // namespace inet

namespace omnetpp {

}  // namespace omnetpp

