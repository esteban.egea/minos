//
// Generated file, do not edit! Created by opp_msgtool 6.0 from inet/applications/vehicular/MinosMCOPacket.msg.
//

#ifndef __INET_MINOSMCOPACKET_M_H
#define __INET_MINOSMCOPACKET_M_H

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wreserved-id-macro"
#endif
#include <omnetpp.h>

// opp_msgtool version check
#define MSGC_VERSION 0x0600
#if (MSGC_VERSION!=OMNETPP_VERSION)
#    error Version mismatch! Probably this file was generated by an earlier version of opp_msgtool: 'make clean' should help.
#endif

// dll export symbol
#ifndef INET_API
#  if defined(INET_EXPORT)
#    define INET_API  OPP_DLLEXPORT
#  elif defined(INET_IMPORT)
#    define INET_API  OPP_DLLIMPORT
#  else
#    define INET_API
#  endif
#endif


namespace inet {

class MinosMCOPacket;

}  // namespace inet

#include "MCOPacket_m.h" // import MCOPacket


namespace inet {

/**
 * Class generated from <tt>inet/applications/vehicular/MinosMCOPacket.msg:20</tt> by opp_msgtool.
 * <pre>
 * class MinosMCOPacket extends MCOPacket
 * {
 *     double channelWeigths[];
 *     double channelCBT[];
 * }
 * </pre>
 */
class INET_API MinosMCOPacket : public ::inet::MCOPacket
{
  protected:
    double *channelWeigths = nullptr;
    size_t channelWeigths_arraysize = 0;
    double *channelCBT = nullptr;
    size_t channelCBT_arraysize = 0;

  private:
    void copy(const MinosMCOPacket& other);

  protected:
    bool operator==(const MinosMCOPacket&) = delete;

  public:
    MinosMCOPacket();
    MinosMCOPacket(const MinosMCOPacket& other);
    virtual ~MinosMCOPacket();
    MinosMCOPacket& operator=(const MinosMCOPacket& other);
    virtual MinosMCOPacket *dup() const override {return new MinosMCOPacket(*this);}
    virtual void parsimPack(omnetpp::cCommBuffer *b) const override;
    virtual void parsimUnpack(omnetpp::cCommBuffer *b) override;

    virtual void setChannelWeigthsArraySize(size_t size);
    virtual size_t getChannelWeigthsArraySize() const;
    virtual double getChannelWeigths(size_t k) const;
    virtual void setChannelWeigths(size_t k, double channelWeigths);
    virtual void insertChannelWeigths(size_t k, double channelWeigths);
    [[deprecated]] void insertChannelWeigths(double channelWeigths) {appendChannelWeigths(channelWeigths);}
    virtual void appendChannelWeigths(double channelWeigths);
    virtual void eraseChannelWeigths(size_t k);

    virtual void setChannelCBTArraySize(size_t size);
    virtual size_t getChannelCBTArraySize() const;
    virtual double getChannelCBT(size_t k) const;
    virtual void setChannelCBT(size_t k, double channelCBT);
    virtual void insertChannelCBT(size_t k, double channelCBT);
    [[deprecated]] void insertChannelCBT(double channelCBT) {appendChannelCBT(channelCBT);}
    virtual void appendChannelCBT(double channelCBT);
    virtual void eraseChannelCBT(size_t k);
};

inline void doParsimPacking(omnetpp::cCommBuffer *b, const MinosMCOPacket& obj) {obj.parsimPack(b);}
inline void doParsimUnpacking(omnetpp::cCommBuffer *b, MinosMCOPacket& obj) {obj.parsimUnpack(b);}


}  // namespace inet


namespace omnetpp {

template<> inline inet::MinosMCOPacket *fromAnyPtr(any_ptr ptr) { return check_and_cast<inet::MinosMCOPacket*>(ptr.get<cObject>()); }

}  // namespace omnetpp

#endif // ifndef __INET_MINOSMCOPACKET_M_H

